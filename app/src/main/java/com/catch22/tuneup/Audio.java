package com.catch22.tuneup;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Handler;
import android.util.Log;

public class Audio
{
    private static final int duration = 100; // seconds
    private static final int sampleRate = 8000;
    private static final int numSamples = duration * sampleRate;
    private static final double sample[] = new double[numSamples];

    private static final byte generatedSnd[] = new byte[2 * numSamples];

    private static final AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
            sampleRate, AudioFormat.CHANNEL_CONFIGURATION_MONO,
            AudioFormat.ENCODING_PCM_16BIT, numSamples,
            AudioTrack.MODE_STATIC);

    private static volatile Thread thread;

    private static Handler handler = new Handler();

    public static void play(final String freq)
    {
        thread = new Thread(new Runnable()
        {
            private double freq;

            public void run()
            {
                freq = Double.valueOf(thread.getName());
                Log.d("Frequency", String.valueOf(freq));
                genTone(freq);
                handler.post(new Runnable()
                {
                    public void run()
                    {
                        playSound();
                    }
                });
            }
        });

        thread.setName(freq);
        thread.start();
    }

    private static void genTone(double freq)
    {
        // fill out the array
        for (int i = 0; i < numSamples; ++i)
            sample[i] = Math.sin(2 * Math.PI * i / (sampleRate/freq));

        // convert to 16 bit pcm sound array
        // assumes the sample buffer is normalised.
        int idx = 0;

        for(final double dVal : sample)
        {
            // scale to maximum amplitude
            final short val = (short) ((dVal * 32767));
            // in 16 bit wav PCM, first byte is the low order byte
            generatedSnd[idx++] = (byte) (val & 0x00ff);
            generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
        }
    }

    private static void playSound()
    {
        audioTrack.write(generatedSnd, 0, generatedSnd.length);
        audioTrack.play();
    }

    public static void stop()
    {
        thread.interrupt();
        audioTrack.stop();
    }
}