package com.catch22.tuneup;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.ToggleButton;

public class TuneUp extends AppCompatActivity implements OnSeekBarChangeListener
{
    //Buttons for standard tuning quick access, and up down togglers
    private static Button[] standardTuningSelectButtons = new Button[6], manualTogglers = new Button[2];

    private static TextView noteText; //the text view which shows the activated note
    public static SeekBar slider; //the user can toggle this slider to change the note

    private static ToggleButton toggleSwitch; //on and off switch to start/stop playing the note

    private static String[] noteMap = {"A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"};
    //frequencies from A1 to E8
    private static String[] frequencies = {"13.75", "14.5", "15.434", "15.980", "16.35","17.32","18.35","19.45","20.60","21.83","23.12","24.50","25.96","27.50","29.14","30.87","32.70","34.65","36.71","38.89","41.20","43.65","46.25","49.00","51.91","55.00","58.27","61.74","65.41","69.30","73.42","77.78","82.41","87.31","92.50","98.00","103.83","110.00","116.54","123.47","130.81","138.59","146.83","155.56","164.81","174.61","185.00","196.00","207.65","220.00","233.08","246.94","261.63","277.18","293.66","311.13","329.63","349.23","369.99","392.00","415.30","440.00","466.16","493.88","523.25","554.37","587.33","622.25","659.25","698.46","739.99","783.99","830.61","880.00","932.33","987.77","1046.50","1108.73","1174.66","1244.51","1318.51","1396.91","1479.98","1567.98","1661.22","1760.00","1864.66","1975.53","2093.00","2217.46","2349.32","2489.02","2637.02","2793.83","2959.96","3135.96","3322.44","3520.00","3729.31","3951.07","4186.01","4434.92","4698.63","4978.03","5274.04","5587.65","5919.91","6271.93","6644.88","7040.00","7458.62","7902.13"};

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tuner); //get the layout

        slider = (SeekBar) findViewById(R.id.noteSlider);  //get the slider from the layout
        noteText = (TextView) findViewById(R.id.noteText); //get the note text from the layout

        toggleSwitch = (ToggleButton) findViewById(R.id.toggleButton); //get the toggle switch from the layout

        slider.setOnSeekBarChangeListener(this); //at an action listener to the seek bar

        slider.setProgress(67); //set the progress of the seek bar to E5

        //action listener for the toggle switch
        toggleSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked) //play sound if on
                    Audio.play(frequencies[slider.getProgress()]);
                else //stop sound if off
                {
                    try
                    {
                        Audio.stop();
                    } catch (Exception threadWasNeverStarted)
                    {

                    }
                }
            }
        });

        manualTogglers[0] = (Button) findViewById(R.id.downButton); //get the down button from the layout
        manualTogglers[1] = (Button) findViewById(R.id.upButton); //get the up button from the layout

        manualTogglers[0].setOnClickListener(new ManualNoteToggler(-1, slider)); //set an action listener to the down button
        manualTogglers[1].setOnClickListener(new ManualNoteToggler(1, slider)); //set an action listener to the up button

        //get the Standard Tuning Quick Access buttons from the layout
        standardTuningSelectButtons[0] = (Button) findViewById(R.id.E5button);
        standardTuningSelectButtons[1] = (Button) findViewById(R.id.B5button);
        standardTuningSelectButtons[2] = (Button) findViewById(R.id.G4button);
        standardTuningSelectButtons[3] = (Button) findViewById(R.id.D4button);
        standardTuningSelectButtons[4] = (Button) findViewById(R.id.A4button);
        standardTuningSelectButtons[5] = (Button) findViewById(R.id.E3button);

        //add action listeners to the Standard Tuning Quick Access buttons
        standardTuningSelectButtons[0].setOnClickListener(new StandardTuningNoteSelector(67));
        standardTuningSelectButtons[1].setOnClickListener(new StandardTuningNoteSelector(62));
        standardTuningSelectButtons[2].setOnClickListener(new StandardTuningNoteSelector(58));
        standardTuningSelectButtons[3].setOnClickListener(new StandardTuningNoteSelector(53));
        standardTuningSelectButtons[4].setOnClickListener(new StandardTuningNoteSelector(48));
        standardTuningSelectButtons[5].setOnClickListener(new StandardTuningNoteSelector(43));
    }

    private class StandardTuningNoteSelector implements OnClickListener //action listener for the Standard Tuning Quick Access buttons
    {
        private int id;

        public StandardTuningNoteSelector(int id)
        {
            this.id = id;
        }

        @Override
        public void onClick(View v)
        {
            slider.setProgress(id); //slide the seek bar to corresponding location
            toggleSwitch.setChecked(true); //set the toggle switch to checked
        }
    }

    private class ManualNoteToggler implements OnClickListener
    {
        private int direction;
        private SeekBar seekBar;

        public ManualNoteToggler(int direction, SeekBar seekBar)
        {
            this.direction = direction;
            this.seekBar = seekBar;
        }

        @Override
        public void onClick(View v)
        {
            seekBar.setProgress(seekBar.getProgress() + direction); //slide the seek bar by -1 or 1 accordingly
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tuner, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
    {
        updateSound(progress); //change the text shown on the active note, and play the sound if the toggle switch is enabled
        Log.d("Progress Changed", String.valueOf(progress));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar)
    {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar)
    {

    }


    public static void updateSound(int progress)
    {
        String note = noteMap[progress % 12]; //first character of the note
        int octave = progress / 12 + 1;
        noteText.setText(note + octave); //change the text of the active note Text View to the concatenation of the three characters

        if(toggleSwitch.isChecked()) //if the toggle switch is on
        {
            try
            {
                Audio.stop(); //stop any audio that was previously playing
            } catch (Exception threadWasNeverStarted)
            {

            }
            try
            {
                Audio.play(frequencies[progress + 12]); //play the selected audio
            } catch(Throwable noteTooHigh)
            {
                Audio.play(frequencies[progress]);
            }
        }
    }
}